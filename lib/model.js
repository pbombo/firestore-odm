const _ = require('lodash')
const Utils = require('./utils')
const InstanceValidator = require('./instance-validator')
const DataTypes = require('./data-types')
// const Op = require('./operators')

/**
 * A Model represents a document in firestore. Instances of this class represent a firestore document.
 *
 * Model instances operate with the concept of a `dataValues` property, which stores the actual values represented by the instance.
 * By default, the values from dataValues can also be accessed directly from the Instance, that is:
 * ```js
 * instance.field
 * // is the same as
 * instance.get('field')
 * // is the same as
 * instance.getDataValue('field')
 * ```
 * However, if getters and/or setters are defined for `field` they will be invoked, instead of returning the value from `dataValues`.
 * Accessing properties directly or using `get` is preferred for regular use, `getDataValue` should only be used for custom getters.
 *
 * @mixes Hooks
 */
class Model {
  /**
   * Builds a new model instance.
   *
   * @param {Object}  [values={}] an object of key value pairs
   * @param {Object}  [options] instance construction options
   * @param {boolean} [options.raw=false] If set to true, values will ignore field and virtual setters.
   * @param {boolean} [options.isNewRecord=true] Is this a new record
   * @param {Array}   [options.include] an array of include options - Used to build prefetched/included model instances. See `set`
   */
  constructor(values = {}, options = {}) {
    options = Object.assign(
      {
        isNewRecord: true,
      },
      options || {}
    )

    if (options.attributes) {
      options.attributes = options.attributes.map(attribute =>
        (Array.isArray(attribute) ? attribute[1] : attribute))
    }

    this.dataValues = {}
    this._previousDataValues = {}
    this._changed = {}
    this._modelOptions = this.constructor.options
    this._options = options || {}

    /**
     * Returns true if this instance has not yet been persisted to the database
     * @property isNewRecord
     * @returns {boolean}
     */
    this.isNewRecord = options.isNewRecord

    this._initValues(values, options)
  }

  _initValues(values, options) {
    let defaults
    let key

    values = (values && _.clone(values)) || {}

    if (options.isNewRecord) {
      defaults = {}

      if (this.constructor._hasDefaultValues) {
        defaults = _.mapValues(this.constructor._defaultValues, valueFn => {
          const value = valueFn()
          return _.cloneDeep(value)
        })
      }

      // set id to null if not passed as value, a newly created dao has no id
      // removing this breaks bulkCreate
      // do after default values since it might have UUID as a default value

      if (
        this.constructor._timestampAttributes.createdAt
        && defaults[this.constructor._timestampAttributes.createdAt]
      ) {
        this.dataValues[
          this.constructor._timestampAttributes.createdAt
        ] = Utils.toDefaultValue(
          defaults[this.constructor._timestampAttributes.createdAt]
        )
        delete defaults[this.constructor._timestampAttributes.createdAt]
      }

      if (
        this.constructor._timestampAttributes.updatedAt
        && defaults[this.constructor._timestampAttributes.updatedAt]
      ) {
        this.dataValues[
          this.constructor._timestampAttributes.updatedAt
        ] = Utils.toDefaultValue(
          defaults[this.constructor._timestampAttributes.updatedAt]
        )
        delete defaults[this.constructor._timestampAttributes.updatedAt]
      }

      if (
        this.constructor._timestampAttributes.deletedAt
        && defaults[this.constructor._timestampAttributes.deletedAt]
      ) {
        this.dataValues[
          this.constructor._timestampAttributes.deletedAt
        ] = Utils.toDefaultValue(
          defaults[this.constructor._timestampAttributes.deletedAt]
        )
        delete defaults[this.constructor._timestampAttributes.deletedAt]
      }

      if (Object.keys(defaults).length) {
        for (key in defaults) {
          if (values[key] === undefined) {
            this.set(key, Utils.toDefaultValue(defaults[key]))
            delete values[key]
          }
        }
      }
    }

    this.set(values, options)
  }

  static _getIncludedAssociation(targetModel, targetAlias) {
    const associations = this.getAssociations(targetModel)
    let association = null
    if (associations.length === 0) {
      throw new Error(`${targetModel.name} is not associated to ${this.name}!`)
    }
    if (associations.length === 1) {
      association = this.getAssociationForAlias(targetModel, targetAlias)
      if (association) {
        return association
      }
      if (targetAlias) {
        const existingAliases = this.getAssociations(targetModel).map(
          association =>
            association.as
        )
        throw new Error(
          `${targetModel.name} is associated to ${this.name} using an alias. `
            + `You've included an alias (${targetAlias}), but it does not match the alias(es) defined in your association (${existingAliases.join(
              ', '
            )}).`
        )
      }
      throw new Error(
        `${targetModel.name} is associated to ${this.name} using an alias. `
          + 'You must use the \'as\' keyword to specify the alias within your include statement.'
      )
    }
    association = this.getAssociationForAlias(targetModel, targetAlias)
    if (!association) {
      throw new Error(
        `${targetModel.name} is associated to ${this.name} multiple times. `
          + 'To identify the correct association, you must use the \'as\' keyword to specify the alias of the association you want to include.'
      )
    }
    return association
  }

  _normalizeAttribute(attribute) {
    if (!_.isPlainObject(attribute)) {
      attribute = { type: attribute }
    }

    if (!attribute.type) return attribute

    attribute.type = this.normalizeDataType(attribute.type)

    if (Object.prototype.hasOwnProperty.call(attribute, 'defaultValue')) {
      if (
        typeof attribute.defaultValue === 'function'
        && (attribute.defaultValue === DataTypes.NOW
          || attribute.defaultValue === DataTypes.UUIDV1
          || attribute.defaultValue === DataTypes.UUIDV4)
      ) {
        attribute.defaultValue = new attribute.defaultValue()
      }
    }

    if (attribute.type instanceof DataTypes.ENUM) {
      // The ENUM is a special case where the type is an object containing the values
      if (attribute.values) {
        attribute.type.values = attribute.type.options.values = attribute.values
      } else {
        attribute.values = attribute.type.values
      }

      if (!attribute.values.length) {
        throw new Error('Values for ENUM have not been defined.')
      }
    }

    return attribute
  }

  /**
   * Initialize a model, representing a table in the DB, with attributes and options.
   *
   * The table columns are defined by the hash that is given as the first argument.
   * Each attribute of the hash represents a column.
   *
   * For more about <a href="/manual/tutorial/models-definition.html#validations"/>Validations</a>
   *
   * More examples, <a href="/manual/tutorial/models-definition.html"/>Model Definition</a>
   *
   * @example
   * Project.init({
   *   columnA: {
   *     type: DataType.BOOLEAN,
   *     validate: {
   *       is: ['[a-z]','i'],        // will only allow letters
   *       max: 23,                  // only allow values <= 23
   *       isIn: {
   *         args: [['en', 'zh']],
   *         msg: "Must be English or Chinese"
   *       }
   *     },
   *     field: 'column_a'
   *     // Other attributes here
   *   },
   *   columnB: DataType.STRING,
   *   columnC: 'MY VERY OWN COLUMN TYPE'
   * })
   *
   * orm.models.modelName // The model will now be available in models under the class name
   *
   * @see
   * {@link DataTypes}
   * @see
   * {@link Hooks}
   *
   * @param {Object}                  attributes An object, where each attribute is a column of the table. Each column can be either a DataType, a string or a type-description object, with the properties described below:
   * @param {string|DataTypes|Object} attributes.column The description of a database column
   * @param {string|DataTypes}        attributes.column.type A string or a data type
   * @param {boolean}                 [attributes.column.allowNull=true] If false, the column will have a NOT NULL constraint, and a not null validation will be run before an instance is saved.
   * @param {any}                     [attributes.column.defaultValue=null] A literal default value, a JavaScript function, or an SQL function
   * @param {string|boolean}          [attributes.column.unique=false] If true, the column will get a unique constraint. If a string is provided, the column will be part of a composite unique index. If multiple columns have the same string, they will be part of the same unique index
   * @param {boolean}                 [attributes.column.primaryKey=false] If true, this attribute will be marked as primary key
   * @param {string}                  [attributes.column.field=null] If set, will map the attribute name to a different name in the database
   * @param {boolean}                 [attributes.column.autoIncrement=false] If true, this column will be set to auto increment
   * @param {string}                  [attributes.column.comment=null] Comment for this column
   * @param {string|Model}            [attributes.column.references=null] An object with reference configurations
   * @param {string|Model}            [attributes.column.references.model] If this column references another table, provide it here as a Model, or a string
   * @param {string}                  [attributes.column.references.key='id'] The column of the foreign table that this column references
   * @param {string}                  [attributes.column.onUpdate] What should happen when the referenced key is updated. One of CASCADE, RESTRICT, SET DEFAULT, SET NULL or NO ACTION
   * @param {string}                  [attributes.column.onDelete] What should happen when the referenced key is deleted. One of CASCADE, RESTRICT, SET DEFAULT, SET NULL or NO ACTION
   * @param {Function}                [attributes.column.get] Provide a custom getter for this column. Use `this.getDataValue(String)` to manipulate the underlying values.
   * @param {Function}                [attributes.column.set] Provide a custom setter for this column. Use `this.setDataValue(String, Value)` to manipulate the underlying values.
   * @param {Object}                  [attributes.column.validate] An object of validations to execute for this column every time the model is saved. Can be either the name of a validation provided by validator.js, a validation function provided by extending validator.js (see the `DAOValidator` property for more details), or a custom validation function. Custom validation functions are called with the value of the field and the instance itself as the `this` binding, and can possibly take a second callback argument, to signal that they are asynchronous. If the validator is sync, it should throw in the case of a failed validation; if it is async, the callback should be called with the error text.
   * @param {Object}                  options These options are merged with the default define options provided to the constructor
   * @param {string}                  [options.modelName] Set name of the model. By default its same as Class name.
   * @param {Object}                  [options.defaultScope={}] Define the default search scope to use for this model. Scopes have the same form as the options passed to find / findAll
   * @param {Object}                  [options.scopes] More scopes, defined in the same way as defaultScope above. See `Model.scope` for more information about how scopes are defined, and what you can do with them
   * @param {boolean}                 [options.omitNull] Don't persist null values. This means that all columns with null values will not be saved
   * @param {boolean}                 [options.timestamps=true] Adds createdAt and updatedAt timestamps to the model.
   * @param {boolean}                 [options.paranoid=false] Calling `destroy` will not delete the model, but instead set a `deletedAt` timestamp if this is true. Needs `timestamps=true` to work
   * @param {boolean}                 [options.underscored=false] Add underscored field to all attributes, this covers user defined attributes, timestamps and foreign keys. Will not affect attributes with explicitly set `field` option
   * @param {boolean}                 [options.freezeTableName=false] If freezeTableName is true, ill not try to alter the model name to get the table name. Otherwise, the model name will be pluralized
   * @param {Object}                  [options.name] An object with two attributes, `singular` and `plural`, which are used when this model is associated to others.
   * @param {string}                  [options.name.singular=Utils.singularize(modelName)] Singular name for model
   * @param {string}                  [options.name.plural=Utils.pluralize(modelName)] Plural name for model
   * @param {Array<Object>}           [options.indexes] indexes definitions
   * @param {string}                  [options.indexes[].name] The name of the index. Defaults to model name + _ + fields concatenated
   * @param {string}                  [options.indexes[].type] Index type. Only used by mysql. One of `UNIQUE`, `FULLTEXT` and `SPATIAL`
   * @param {string}                  [options.indexes[].using] The method to create the index by (`USING` statement in SQL). BTREE and HASH are supported by mysql and postgres, and postgres additionally supports GIST and GIN.
   * @param {string}                  [options.indexes[].operator] Specify index operator.
   * @param {boolean}                 [options.indexes[].unique=false] Should the index by unique? Can also be triggered by setting type to `UNIQUE`
   * @param {boolean}                 [options.indexes[].concurrently=false] PostgresSQL will build the index without taking any write locks. Postgres only
   * @param {Array<string|Object>}    [options.indexes[].fields] An array of the fields to index. Each field can either be a string containing the name of the field, an object or an object with the following attributes: `attribute` (field name), `length` (create a prefix index of length chars), `order` (the direction the column should be sorted in), `collate` (the collation (sort order) for the column)
   * @param {string|boolean}          [options.createdAt] Override the name of the createdAt attribute if a string is provided, or disable it if false. Timestamps must be true. Underscored field will be set with underscored setting.
   * @param {string|boolean}          [options.updatedAt] Override the name of the updatedAt attribute if a string is provided, or disable it if false. Timestamps must be true. Underscored field will be set with underscored setting.
   * @param {string|boolean}          [options.deletedAt] Override the name of the deletedAt attribute if a string is provided, or disable it if false. Timestamps must be true. Underscored field will be set with underscored setting.
   * @param {string}                  [options.collectionName] Defaults to pluralized model name, unless freezeTableName is true, in which case it uses model name verbatim
   * @param {string}                  [options.schema='public'] schema
   * @param {string}                  [options.engine] Specify engine for model's table
   * @param {string}                  [options.charset] Specify charset for model's table
   * @param {string}                  [options.comment] Specify comment for model's table
   * @param {string}                  [options.collate] Specify collation for model's table
   * @param {string}                  [options.initialAutoIncrement] Set the initial AUTO_INCREMENT value for the table in MySQL.
   * @param {Object}                  [options.hooks] An object of hook function that are called before and after certain lifecycle events. The possible hooks are: beforeValidate, afterValidate, validationFailed, beforeBulkCreate, beforeBulkDestroy, beforeBulkUpdate, beforeCreate, beforeDestroy, beforeUpdate, afterCreate, beforeSave, afterDestroy, afterUpdate, afterBulkCreate, afterSave, afterBulkDestroy and afterBulkUpdate. See Hooks for more information about hook functions and their signatures. Each property can either be a function, or an array of functions.
   * @param {Object}                  [options.validate] An object of model wide validations. Validations have access to all model values via `this`. If the validator function takes an argument, it is assumed to be async, and is called with a callback that accepts an optional error.
   *
   * @returns {Model}
   */
  static init(attributes, options = {}) {
    // options = Utils.merge(_.cloneDeep(globalOptions.define), options);
    if (!Object.keys(options).some(opt =>
      opt === 'db')) {
      throw Error('DB must be defined in options')
    }

    this.db = options.db

    if (!options.modelName) {
      options.modelName = this.name
    }

    options = Utils.merge(
      {
        name: {
          plural: Utils.pluralize(options.modelName),
          singular: Utils.singularize(options.modelName),
        },
      },
      options
    )

    delete options.modelName

    this.options = Object.assign(
      {
        timestamps: true,
        validate: {},
        underscored: false,
        paranoid: false,
        rejectOnEmpty: false,
        whereCollection: null,
      },
      options
    )

    this.associations = {}
    // this._setupHooks(options.hooks);

    this.underscored = this.options.underscored

    if (!this.options.collectionName) {
      this.collectionName = this.options.freezeTableName
        ? this.name
        : Utils.underscoredIf(Utils.pluralize(this.name), this.underscored)
    } else {
      this.collectionName = this.options.collectionName
    }

    this._schema = this.options.schema
    this._schemaDelimiter = this.options.schemaDelimiter

    // error check options
    _.each(options.validate, (validator, validatorType) => {
      if (Object.prototype.hasOwnProperty.call(attributes, validatorType)) {
        throw new Error(
          `A model validator function must not have the same name as a field. Model: ${this.name}, field/validation name: ${validatorType}`
        )
      }

      if (typeof validator !== 'function') {
        throw new Error(
          `Members of the validate option must be functions. Model: ${this.name}, error with validate member ${validatorType}`
        )
      }
    })

    this.rawAttributes = _.mapValues(attributes, (attribute, name) => {
      // attribute = this._normalizeAttribute(attribute);

      if (attribute.type === undefined) {
        throw new Error(
          `Unrecognized datatype for attribute "${this.name}.${name}"`
        )
      }

      if (
        attribute.allowNull !== false
        && _.get(attribute, 'validate.notNull')
      ) {
        throw new Error(
          `Invalid definition for "${this.name}.${name}", "notNull" validator is only allowed with "allowNull:false"`
        )
      }

      if (_.get(attribute, 'references.model.prototype') instanceof Model) {
        attribute.references.model = attribute.references.model.getDocumentName()
      }

      return attribute
    })

    // const collectionName = this.getTableName();
    // this._indexes = this.options.indexes
    //   .map(index => Utils.nameIndex(this._conformIndex(index), collectionName));

    this.primaryKeys = {}
    this._readOnlyAttributes = new Set()
    this._timestampAttributes = {}

    // setup names of timestamp attributes
    if (this.options.timestamps) {
      if (this.options.createdAt !== false) {
        this._timestampAttributes.createdAt = this.options.createdAt || 'createdAt'
        this._readOnlyAttributes.add(this._timestampAttributes.createdAt)
      }
      if (this.options.updatedAt !== false) {
        this._timestampAttributes.updatedAt = this.options.updatedAt || 'updatedAt'
        this._readOnlyAttributes.add(this._timestampAttributes.updatedAt)
      }
      if (this.options.paranoid && this.options.deletedAt !== false) {
        this._timestampAttributes.deletedAt = this.options.deletedAt || 'deletedAt'
        this._readOnlyAttributes.add(this._timestampAttributes.deletedAt)
      }
    }

    // setup name for version attribute
    if (this.options.version) {
      this._versionAttribute = typeof this.options.version === 'string'
        ? this.options.version
        : 'version'
      this._readOnlyAttributes.add(this._versionAttribute)
    }

    this._hasReadOnlyAttributes = this._readOnlyAttributes.size > 0

    // Add head and tail default attributes (id, timestamps)
    this._addDefaultAttributes()
    this.refreshAttributes()

    // this.sequelize.runHooks('afterDefine', this);

    return this
  }

  static refreshAttributes() {
    const attributeManipulation = {}

    this.prototype._customGetters = {}
    this.prototype._customSetters = {};
    ['get', 'set'].forEach(type => {
      const opt = `${type}terMethods`
      const funcs = _.clone(
        _.isObject(this.options[opt]) ? this.options[opt] : {}
      )
      const _custom = type === 'get'
        ? this.prototype._customGetters
        : this.prototype._customSetters

      _.each(funcs, (method, attribute) => {
        _custom[attribute] = method

        if (type === 'get') {
          funcs[attribute] = function () {
            return this.get(attribute)
          }
        }
        if (type === 'set') {
          funcs[attribute] = function (value) {
            return this.set(attribute, value)
          }
        }
      })

      _.each(this.rawAttributes, (options, attribute) => {
        if (Object.prototype.hasOwnProperty.call(options, type)) {
          _custom[attribute] = options[type]
        }

        if (type === 'get') {
          funcs[attribute] = function () {
            return this.get(attribute)
          }
        }
        if (type === 'set') {
          funcs[attribute] = function (value) {
            return this.set(attribute, value)
          }
        }
      })

      _.each(funcs, (fct, name) => {
        if (!attributeManipulation[name]) {
          attributeManipulation[name] = {
            configurable: true,
          }
        }
        attributeManipulation[name][type] = fct
      })
    })

    this._dataTypeChanges = {}
    this._dataTypeSanitizers = {}

    this._hasBooleanAttributes = false
    this._hasDateAttributes = false
    this._jsonAttributes = new Set()
    this._virtualAttributes = new Set()
    this._defaultValues = {}
    this.prototype.validators = {}

    this.fieldRawAttributesMap = {}

    this.primaryKeys = {}
    this.uniqueKeys = {}

    _.each(this.rawAttributes, (definition, name) => {
      // definition.type = this.sequelize.normalizeDataType(definition.type);

      definition.Model = this
      definition.fieldName = name
      definition._modelAttribute = true

      if (definition.field === undefined) {
        definition.field = Utils.underscoredIf(name, this.underscored)
      }

      if (definition.primaryKey === true) {
        this.primaryKeys[name] = definition
      }

      this.fieldRawAttributesMap[definition.field] = definition

      if (definition.type._sanitize) {
        this._dataTypeSanitizers[name] = definition.type._sanitize
      }

      if (definition.type._isChanged) {
        this._dataTypeChanges[name] = definition.type._isChanged
      }

      if (definition.type instanceof DataTypes.BOOLEAN) {
        this._hasBooleanAttributes = true
      } else if (
        definition.type instanceof DataTypes.DATE
        || definition.type instanceof DataTypes.DATEONLY
      ) {
        this._hasDateAttributes = true
      }

      if (
        Object.prototype.hasOwnProperty.call(definition, 'unique')
        && definition.unique
      ) {
        let idxName
        if (
          typeof definition.unique === 'object'
          && Object.prototype.hasOwnProperty.call(definition.unique, 'name')
        ) {
          idxName = definition.unique.name
        } else if (typeof definition.unique === 'string') {
          idxName = definition.unique
        } else {
          idxName = `${this.constructor.colName}_${name}_unique`
        }

        const idx = this.uniqueKeys[idxName] || { fields: [] }

        idx.fields.push(definition.field)
        idx.msg = idx.msg || definition.unique.msg || null
        idx.name = idxName || false
        idx.column = name
        idx.customIndex = definition.unique !== true

        this.uniqueKeys[idxName] = idx
      }

      if (Object.prototype.hasOwnProperty.call(definition, 'validate')) {
        this.prototype.validators[name] = definition.validate
      }

      if (
        definition.index === true
        && definition.type instanceof DataTypes.JSONB
      ) {
        this._indexes.push(
          Utils.nameIndex(
            this._conformIndex({
              fields: [definition.field || name],
              using: 'gin',
            }),
            this.getDocumentName()
          )
        )

        delete definition.index
      }
    })

    // Create a map of field to attribute names
    this.fieldAttributeMap = _.reduce(
      this.fieldRawAttributesMap,
      (map, value, key) => {
        if (key !== value.fieldName) {
          map[key] = value.fieldName
        }
        return map
      },
      {}
    )

    this._hasJsonAttributes = !!this._jsonAttributes.size

    this._hasVirtualAttributes = !!this._virtualAttributes.size

    this._hasDefaultValues = !_.isEmpty(this._defaultValues)

    this.tableAttributes = _.omitBy(this.rawAttributes, (_a, key) =>
      this._virtualAttributes.has(key))

    this.prototype._hasCustomGetters = Object.keys(
      this.prototype._customGetters
    ).length
    this.prototype._hasCustomSetters = Object.keys(
      this.prototype._customSetters
    ).length

    for (const key of Object.keys(attributeManipulation)) {
      if (Object.prototype.hasOwnProperty.call(Model.prototype, key)) {
        console.log(
          `Not overriding built-in method from model attribute: ${key}`
        )
        continue
      }
      Object.defineProperty(this.prototype, key, attributeManipulation[key])
    }

    this.prototype.rawAttributes = this.rawAttributes
    this.prototype._isAttribute = key =>
      Object.prototype.hasOwnProperty.call(this.prototype.rawAttributes, key)

    // Primary key convenience constiables
    this.primaryKeyAttributes = Object.keys(this.primaryKeys)
    this.primaryKeyAttribute = this.primaryKeyAttributes[0]
    if (this.primaryKeyAttribute) {
      this.primaryKeyField = this.rawAttributes[this.primaryKeyAttribute].field
        || this.primaryKeyAttribute
    }

    this._hasPrimaryKeys = this.primaryKeyAttributes.length > 0
    this._isPrimaryKey = key =>
      this.primaryKeyAttributes.includes(key)
  }

  static _addDefaultAttributes() {
    const tail = {}
    let head = {}

    // Add id if no primary key was manually added to definition
    // Can't use this.primaryKeys here, since this function is called before PKs are identified
    if (!_.some(this.rawAttributes, 'primaryKey')) {
      if ('id' in this.rawAttributes) {
        // Something is fishy here!
        throw new Error(
          `A column called 'id' was added to the attributes of '${this.collectionName}' but not marked with 'primaryKey: true'`
        )
      }

      head = {
        id: {
          type: new DataTypes.UUIDV4(),
          allowNull: false,
          primaryKey: true,
          _autoGenerated: true,
        },
      }
    }

    if (this._timestampAttributes.createdAt) {
      tail[this._timestampAttributes.createdAt] = {
        type: DataTypes.DATE,
        allowNull: false,
        _autoGenerated: true,
      }
    }

    if (this._timestampAttributes.updatedAt) {
      tail[this._timestampAttributes.updatedAt] = {
        type: DataTypes.DATE,
        allowNull: false,
        _autoGenerated: true,
      }
    }

    if (this._timestampAttributes.deletedAt) {
      tail[this._timestampAttributes.deletedAt] = {
        type: DataTypes.DATE,
        _autoGenerated: true,
      }
    }

    if (this._versionAttribute) {
      tail[this._versionAttribute] = {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
        _autoGenerated: true,
      }
    }

    const existingAttributes = _.clone(this.rawAttributes)
    this.rawAttributes = {}

    _.each(head, (value, attr) => {
      this.rawAttributes[attr] = value
    })

    _.each(existingAttributes, (value, attr) => {
      this.rawAttributes[attr] = value
    })

    _.each(tail, (value, attr) => {
      if (this.rawAttributes[attr] === undefined) {
        this.rawAttributes[attr] = value
      }
    })

    if (!Object.keys(this.primaryKeys).length) {
      this.primaryKeys.id = this.rawAttributes.id
    }
  }

  getDocumentName() {
    return this._modelOptions.collectionName || this._modelOptions.name.plural
  }

  /**
   * Get the value of the underlying data value
   *
   * @param {string} key key to look in instance data store
   *
   * @returns {any}
   */
  getDataValue(key) {
    return this.dataValues[key]
  }

  /**
   * Update the underlying data value
   *
   * @param {string} key key to set in instance data store
   * @param {any} value new value for given key
   *
   */
  setDataValue(key, value) {
    const originalValue = this._previousDataValues[key]

    if (
      (!Utils.isPrimitive(value) && value !== null)
      || value !== originalValue
    ) {
      this.changed(key, true)
    }

    this.dataValues[key] = value
  }

  /**
   * If no key is given, returns all values of the instance, also invoking virtual getters.
   *
   * If key is given and a field or virtual getter is present for the key it will call that getter - else it will return the value for key.
   *
   * @param {string}  [key] key to get value of
   * @param {Object}  [options] get options
   * @param {boolean} [options.plain=false] If set to true, included instances will be returned as plain objects
   * @param {boolean} [options.raw=false] If set to true, field and virtual setters will be ignored
   *
   * @returns {Object|any}
   */
  get(key, options) {
    if (options === undefined && typeof key === 'object') {
      options = key
      key = undefined
    }

    options = options || {}

    if (key) {
      if (
        Object.prototype.hasOwnProperty.call(this._customGetters, key)
        && !options.raw
      ) {
        return this._customGetters[key].call(this, key, options)
      }

      if (
        options.plain
        && this._options.include
        && this._options.includeNames.includes(key)
      ) {
        if (Array.isArray(this.dataValues[key])) {
          return this.dataValues[key].map(instance =>
            instance.get(options))
        }
        if (this.dataValues[key] instanceof Model) {
          return this.dataValues[key].get(options)
        }
        return this.dataValues[key]
      }

      return this.dataValues[key]
    }

    if (
      this._hasCustomGetters
      || (options.plain && this._options.include)
      || options.clone
    ) {
      const values = {}
      let _key

      if (this._hasCustomGetters) {
        for (_key in this._customGetters) {
          if (
            this._options.attributes
            && !this._options.attributes.includes(_key)
          ) {
            continue
          }

          if (Object.prototype.hasOwnProperty.call(this._customGetters, _key)) {
            values[_key] = this.get(_key, options)
          }
        }
      }

      for (_key in this.dataValues) {
        if (
          !Object.prototype.hasOwnProperty.call(values, _key)
          && Object.prototype.hasOwnProperty.call(this.dataValues, _key)
        ) {
          values[_key] = this.get(_key, options)
        }
      }

      return values
    }

    return this.dataValues
  }

  /**
   * Set is used to update values on the instance (the representation of the instance that is, remember that nothing will be persisted before you actually call `save`).
   * In its most basic form `set` will update a value stored in the underlying `dataValues` object. However, if a custom setter function is defined for the key, that function
   * will be called instead. To bypass the setter, you can pass `raw: true` in the options object.
   *
   * If set is called with an object, it will loop over the object, and call set recursively for each key, value pair. If you set raw to true, the underlying dataValues will either be
   * set directly to the object passed, or used to extend dataValues, if dataValues already contain values.
   *
   * When set is called, the previous value of the field is stored and sets a changed flag(see `changed`).
   *
   * Set can also be used to build instances for associations, if you have values for those.
   * When using set with associations you need to make sure the property key matches the alias of the association
   * while also making sure that the proper include options have been set (from .build() or .findOne())
   *
   * If called with a dot.separated key on a JSON/JSONB attribute it will set the value nested and flag the entire object as changed.
   *
   * @see
   * {@link Model.findAll} for more information about includes
   *
   * @param {string|Object} key key to set, it can be string or object. When string it will set that key, for object it will loop over all object properties nd set them.
   * @param {any} value value to set
   * @param {Object} [options] set options
   * @param {boolean} [options.raw=false] If set to true, field and virtual setters will be ignored
   * @param {boolean} [options.reset=false] Clear all previously set data values
   *
   * @returns {Model}
   */
  set(key, value, options) {
    let values
    let originalValue

    if (typeof key === 'object' && key !== null) {
      values = key
      options = value || {}

      if (options.reset) {
        this.dataValues = {}
        for (const key in values) {
          this.changed(key, false)
        }
      }

      // If raw, and we're not dealing with includes or special attributes, just set it straight on the dataValues object
      if (
        options.raw
        && !(this._options && this._options.include)
        && !(options && options.attributes)
        && !this.constructor._hasDateAttributes
        && !this.constructor._hasBooleanAttributes
      ) {
        if (Object.keys(this.dataValues).length) {
          this.dataValues = Object.assign(this.dataValues, values)
        } else {
          this.dataValues = values
        }
        // If raw, .changed() shouldn't be true
        this._previousDataValues = _.clone(this.dataValues)
      } else {
        // Loop and call set
        if (options.attributes) {
          const setKeys = data => {
            for (const k of data) {
              if (values[k] === undefined) {
                continue
              }
              this.set(k, values[k], options)
            }
          }
          setKeys(options.attributes)
          if (this.constructor._hasVirtualAttributes) {
            setKeys(this.constructor._virtualAttributes)
          }
          if (this._options.includeNames) {
            setKeys(this._options.includeNames)
          }
        } else {
          for (const key in values) {
            this.set(key, values[key], options)
          }
        }

        if (options.raw) {
          // If raw, .changed() shouldn't be true
          this._previousDataValues = _.clone(this.dataValues)
        }
      }
      return this
    }
    if (!options) options = {}
    if (!options.raw) {
      originalValue = this.dataValues[key]
    }

    // Check if we have included models, and if this key matches the include model names/aliases
    if (
      this._options
      && this._options.include
      && this._options.includeNames.includes(key)
    ) {
      // Pass it on to the include handler
      this._setInclude(key, value, options)
      return this
    }
    // Bunch of stuff we won't do when it's raw
    if (!options.raw) {
      // If attribute is not in model definition, return
      if (!this._isAttribute(key)) {
        if (
          key.includes('.')
          && this.constructor._jsonAttributes.has(key.split('.')[0])
        ) {
          const previousNestedValue = Dottie.get(this.dataValues, key)
          if (!_.isEqual(previousNestedValue, value)) {
            Dottie.set(this.dataValues, key, value)
            this.changed(key.split('.')[0], true)
          }
        }
        return this
      }

      // If attempting to set primary key and primary key is already defined, return
      if (
        this.constructor._hasPrimaryKeys
        && originalValue
        && this.constructor._isPrimaryKey(key)
      ) {
        return this
      }

      // If attempting to set read only attributes, return
      if (
        !this.isNewRecord
        && this.constructor._hasReadOnlyAttributes
        && this.constructor._readOnlyAttributes.has(key)
      ) {
        return this
      }
    }

    // If there's a data type sanitizer
    // if (
    //   !(value instanceof Utils.SequelizeMethod)
    //   && Object.prototype.hasOwnProperty.call(this.constructor._dataTypeSanitizers, key)
    // ) {
    //   value = this.constructor._dataTypeSanitizers[key].call(this, value, options);
    // }

    // Set when the value has changed and not raw
    if (
      !options.raw
      // Check default
      && (!this.constructor._dataTypeChanges[key]
        && ((!Utils.isPrimitive(value) && value !== null)
          || value !== originalValue))
    ) {
      this._previousDataValues[key] = originalValue
      this.changed(key, true)
    }

    // set data value
    this.dataValues[key] = value
    return this
  }

  /**
   * Destroy the row corresponding to this instance. Depending on your setting for paranoid, the row will either be completely deleted, or have its deletedAt timestamp set to the current time.
   *
   * @param {Object}      [options={}] destroy options
   * @param {boolean}     [options.force=false] If set to true, paranoid models will actually be deleted
   * @param {Function}    [options.logging=false] A function that gets executed while running the query to log the sql.
   * @param {Transaction} [options.transaction] Transaction to run query under
   * @param {string}      [options.searchPath=DEFAULT] An optional parameter to specify the schema search_path (Postgres only)
   *
   * @returns {Promise}
   */
  destroy(options) {
    options = Object.assign(
      {
        // hooks: true,
        force: false,
      },
      options
    )

    if (
      this.constructor._timestampAttributes.deletedAt
      && options.force === false
    ) {
      const attribute = this.constructor.rawAttributes[
        this.constructor._timestampAttributes.deletedAt
      ]
      const field = attribute.field || this.constructor._timestampAttributes.deletedAt
      const values = Utils.mapValueFieldNames(
        this.dataValues,
        this.changed() || [],
        this.constructor
      )

      values[field] = new Date().toJSON()
      this.setDataValue(field, values[field])
      return this.save()
    }
    const ref = this.constructor.db.collection(this.getDocumentName())
    return ref
      .doc(this.id)
      .delete()
      .then(_ =>
        true)
      .catch(_ =>
        false)
  }

  /**
   * Creates a 1:m association between this (the source) and the provided target.
   * The foreign key is added on the target.
   *
   * @param {Model}               target Target model
   * @param {Object}              [options] hasMany association options
   * @param {boolean}             [options.hooks=false] Set to true to run before-/afterDestroy hooks when an associated model is deleted because of a cascade. For example if `User.hasOne(Profile, {onDelete: 'cascade', hooks:true})`, the before-/afterDestroy hooks for profile will be called when a user is deleted. Otherwise the profile will be deleted without invoking any hooks
   * @param {string|Object}       [options.as] The alias of this model. If you provide a string, it should be plural, and will be singularized using node.inflection. If you want to control the singular version yourself, provide an object with `plural` and `singular` keys. See also the `name` option passed to `define`. If you create multiple associations between the same tables, you should provide an alias to be able to distinguish between them. If you provide an alias when creating the association, you should provide the same alias when eager loading and when getting associated models. Defaults to the pluralized name of target
   * @param {string|Object}       [options.foreignKey] The name of the foreign key in the target table or an object representing the type definition for the foreign column. When using an object, you can add a `name` property to set the name of the column. Defaults to the name of source + primary key of source
   * @param {string}              [options.sourceKey] The name of the field to use as the key for the association in the source table. Defaults to the primary key of the source table
   * @param {Object}              [options.scope] A key/value set that will be used for association create and find defaults on the target. (sqlite not supported for N:M)
   * @param {string}              [options.onDelete='SET&nbsp;NULL|CASCADE'] SET NULL if foreignKey allows nulls, CASCADE if otherwise
   * @param {string}              [options.onUpdate='CASCADE'] Set `ON UPDATE`
   * @param {boolean}             [options.constraints=true] Should on update and on delete constraints be enabled on the foreign key.
   *
   * @returns {HasMany}
   *
   * @example
   * User.hasMany(Profile) // This will add userId to the profile table
   */
  static hasMany(target, options) {} // eslint-disable-line

  /**
   * Create an N:M association with a join table. Defining `through` is required.
   *
   * @param {Model}               target Target model
   * @param {Object}              options belongsToMany association options
   * @param {boolean}             [options.hooks=false] Set to true to run before-/afterDestroy hooks when an associated model is deleted because of a cascade. For example if `User.hasOne(Profile, {onDelete: 'cascade', hooks:true})`, the before-/afterDestroy hooks for profile will be called when a user is deleted. Otherwise the profile will be deleted without invoking any hooks
   * @param {Model|string|Object} options.through The name of the table that is used to join source and target in n:m associations. Can also be a model if you want to define the junction table yourself and add extra attributes to it.
   * @param {Model}               [options.through.model] The model used to join both sides of the N:M association.
   * @param {Object}              [options.through.scope] A key/value set that will be used for association create and find defaults on the through model. (Remember to add the attributes to the through model)
   * @param {boolean}             [options.through.unique=true] If true a unique key will be generated from the foreign keys used (might want to turn this off and create specific unique keys when using scopes)
   * @param {string|Object}       [options.as] The alias of this association. If you provide a string, it should be plural, and will be singularized using node.inflection. If you want to control the singular version yourself, provide an object with `plural` and `singular` keys. See also the `name` option passed to `define`. If you create multiple associations between the same tables, you should provide an alias to be able to distinguish between them. If you provide an alias when creating the association, you should provide the same alias when eager loading and when getting associated models. Defaults to the pluralized name of target
   * @param {string|Object}       [options.foreignKey] The name of the foreign key in the join table (representing the source model) or an object representing the type definition for the foreign column (see `define` for syntax). When using an object, you can add a `name` property to set the name of the column. Defaults to the name of source + primary key of source
   * @param {string|Object}       [options.otherKey] The name of the foreign key in the join table (representing the target model) or an object representing the type definition for the other column (see `define` for syntax). When using an object, you can add a `name` property to set the name of the column. Defaults to the name of target + primary key of target
   * @param {Object}              [options.scope] A key/value set that will be used for association create and find defaults on the target. (sqlite not supported for N:M)
   * @param {boolean}             [options.timestamps=options.timestamps] Should the join model have timestamps
   * @param {string}              [options.onDelete='SET&nbsp;NULL|CASCADE'] Cascade if this is a n:m, and set null if it is a 1:m
   * @param {string}              [options.onUpdate='CASCADE'] Sets `ON UPDATE`
   * @param {boolean}             [options.constraints=true] Should on update and on delete constraints be enabled on the foreign key.
   *
   * @returns {BelongsToMany}
   *
   * @example
   * // Automagically generated join model
   * User.belongsToMany(Project, { through: 'UserProjects' })
   * Project.belongsToMany(User, { through: 'UserProjects' })
   *
   * // Join model with additional attributes
   * const UserProjects = firestoreOrm.define('UserProjects', {
   *   started: DataType.BOOLEAN
   * })
   * User.belongsToMany(Project, { through: UserProjects })
   * Project.belongsToMany(User, { through: UserProjects })
   */
  static belongsToMany(target, options) {} // eslint-disable-line

  /**
   * Creates an association between this (the source) and the provided target. The foreign key is added on the target.
   *
   * @param {Model}           target Target model
   * @param {Object}          [options] hasOne association options
   * @param {boolean}         [options.hooks=false] Set to true to run before-/afterDestroy hooks when an associated model is deleted because of a cascade. For example if `User.hasOne(Profile, {onDelete: 'cascade', hooks:true})`, the before-/afterDestroy hooks for profile will be called when a user is deleted. Otherwise the profile will be deleted without invoking any hooks
   * @param {string}          [options.as] The alias of this model, in singular form. See also the `name` option passed to `define`. If you create multiple associations between the same tables, you should provide an alias to be able to distinguish between them. If you provide an alias when creating the association, you should provide the same alias when eager loading and when getting associated models. Defaults to the singularized name of target
   * @param {string|Object}   [options.foreignKey] The name of the foreign key attribute in the target model or an object representing the type definition for the foreign column (see `define` for syntax). When using an object, you can add a `name` property to set the name of the column. Defaults to the name of source + primary key of source
   * @param {string}          [options.sourceKey] The name of the attribute to use as the key for the association in the source table. Defaults to the primary key of the source table
   * @param {string}          [options.onDelete='SET&nbsp;NULL|CASCADE'] SET NULL if foreignKey allows nulls, CASCADE if otherwise
   * @param {string}          [options.onUpdate='CASCADE'] Sets 'ON UPDATE'
   * @param {boolean}         [options.constraints=true] Should on update and on delete constraints be enabled on the foreign key.
   * @param {string}          [options.uniqueKey] The custom name for unique constraint.
   *
   * @returns {HasOne}
   *
   * @example
   * User.hasOne(Profile) // This will add userId to the profile table
   */
  static hasOne(target, options) {} // eslint-disable-line

  /**
   * Creates an association between this (the source) and the provided target. The foreign key is added on the source.
   *
   * @param {Model}           target The target model
   * @param {Object}          [options] belongsTo association options
   * @param {boolean}         [options.hooks=false] Set to true to run before-/afterDestroy hooks when an associated model is deleted because of a cascade. For example if `User.hasOne(Profile, {onDelete: 'cascade', hooks:true})`, the before-/afterDestroy hooks for profile will be called when a user is deleted. Otherwise the profile will be deleted without invoking any hooks
   * @param {string}          [options.as] The alias of this model, in singular form. See also the `name` option passed to `define`. If you create multiple associations between the same tables, you should provide an alias to be able to distinguish between them. If you provide an alias when creating the association, you should provide the same alias when eager loading and when getting associated models. Defaults to the singularized name of target
   * @param {string|Object}   [options.foreignKey] The name of the foreign key attribute in the source table or an object representing the type definition for the foreign column (see `define` for syntax). When using an object, you can add a `name` property to set the name of the column. Defaults to the name of target + primary key of target
   * @param {string}          [options.targetKey] The name of the attribute to use as the key for the association in the target table. Defaults to the primary key of the target table
   * @param {string}          [options.onDelete='SET&nbsp;NULL|NO&nbsp;ACTION'] SET NULL if foreignKey allows nulls, NO ACTION if otherwise
   * @param {string}          [options.onUpdate='CASCADE'] Sets 'ON UPDATE'
   * @param {boolean}         [options.constraints=true] Should on update and on delete constraints be enabled on the foreign key.
   *
   * @returns {BelongsTo}
   *
   * @example
   * Profile.belongsTo(User) // This will add userId to the profile table
   */
  static belongsTo(target, options) {} // eslint-disable-line

  /**
   * Builds a new model instance and calls save on it.

   * @see
   * {@link Model.build}
   * @see
   * {@link Model.save}
   *
   * @param {Object}        values hash of data values to create new record with
   * @param {Object}        [options] build and query options
   * @param {boolean}       [options.raw=false] If set to true, values will ignore field and virtual setters.
   * @param {boolean}       [options.isNewRecord=true] Is this new record
   * @param {Array}         [options.include] an array of include options - Used to build prefetched/included model instances. See `set`
   * @param {Array}         [options.fields] If set, only columns matching those in fields will be saved
   * @param {string[]}      [options.fields] An optional array of strings, representing database columns. If fields is provided, only those columns will be validated and saved.
   * @param {boolean}       [options.silent=false] If true, the updatedAt timestamp will not be updated.
   * @param {boolean}       [options.validate=true] If false, validations won't be run.
   * @param {boolean}       [options.hooks=true] Run before and after create / update + validate hooks
   * @param {Function}      [options.logging=false] A function that gets executed while running the query to log the sql.
   * @param {boolean}       [options.benchmark=false] Pass query execution time in milliseconds as second argument to logging function (options.logging).
   * @param {Transaction}   [options.transaction] Transaction to run query under
   * @param {string}        [options.searchPath=DEFAULT] An optional parameter to specify the schema search_path (Postgres only)
   * @param {boolean}       [options.returning=true] Return the affected rows (only for postgres)
   *
   * @returns {Promise<Model>}
   *
   */
  static create(values, options) {
    options = Utils.cloneDeep(options || {})

    return this.build(values, {
      isNewRecord: true,
      attributes: options.fields,
      include: options.include,
      raw: options.raw,
      silent: options.silent,
    }).save(options)
  }

  /**
   * Delete multiple instances, or set their deletedAt timestamp to the current time if `paranoid` is enabled.
   *
   * @param  {Object}       options                         destroy options
   * @param  {Object}       [options.where]                 Filter the destroy
   * @param  {boolean}      [options.hooks=true]            Run before / after bulk destroy hooks?
   * @param  {boolean}      [options.individualHooks=false] If set to true, destroy will SELECT all records matching the where parameter and will execute before / after destroy hooks on each row
   * @param  {number}       [options.limit]                 How many rows to delete
   * @param  {boolean}      [options.force=false]           Delete instead of setting deletedAt to current timestamp (only applicable if `paranoid` is enabled)
   * @param  {boolean}      [options.truncate=false]        If set to true, dialects that support it will use TRUNCATE instead of DELETE FROM. If a table is truncated the where and limit options are ignored
   * @param  {boolean}      [options.cascade=false]         Only used in conjunction with TRUNCATE. Truncates  all tables that have foreign-key references to the named table, or to any tables added to the group due to CASCADE.
   * @param  {boolean}      [options.restartIdentity=false] Only used in conjunction with TRUNCATE. Automatically restart sequences owned by columns of the truncated table.
   * @param  {Transaction}  [options.transaction] Transaction to run query under
   * @param  {Function}     [options.logging=false]         A function that gets executed while running the query to log the sql.
   * @param  {boolean}      [options.benchmark=false]       Pass query execution time in milliseconds as second argument to logging function (options.logging).
   *
   * @returns {Promise<number>} The number of destroyed rows
   */
  static destroy(options) {
    options = Utils.cloneDeep(options)
    this._injectScope(options)

    if (!options || !(options.where || options.truncate)) {
      throw new Error(
        'Missing where or truncate attribute in the options parameter of model.destroy.'
      )
    }

    if (
      !options.truncate
      && !_.isPlainObject(options.where)
      && !Array.isArray(options.where)
    ) {
      throw new Error(
        'Expected plain object, array or method in the options.where parameter of model.destroy.'
      )
    }

    options = _.defaults(options, {
      // hooks: true,
      individualHooks: false,
      force: false,
      cascade: false,
      restartIdentity: false,
    })

    Utils.mapOptionFieldNames(options, this)
    options.model = this

    const ref = this.constructor.db.collection(this.getDocumentName())
    if (this._timestampAttributes.deletedAt && !options.force) {
      ref
        .doc(this.id)
        .update({ deletedAt: new Date().toJSON() })
        .then(_ =>
          true)
        .catch(_ =>
          false)
    } else {
      return ref
        .doc(this.id)
        .delete()
        .then(_ =>
          true)
        .catch(_ =>
          false)
    }

    return this
  }

  /**
   * Builds a new model instance.
   *
   * @param {Object|Array} values An object of key value pairs or an array of such. If an array, the function will return an array of instances.
   * @param {Object}  [options] Instance build options
   * @param {boolean} [options.raw=false] If set to true, values will ignore field and virtual setters.
   * @param {boolean} [options.isNewRecord=true] Is this new record
   * @param {Array}   [options.include] an array of include options - Used to build prefetched/included model instances. See `set`
   *
   * @returns {Model|Array<Model>}
   */
  static build(values, options) {
    if (Array.isArray(values)) {
      return this.bulkBuild(values, options)
    }

    return new this(values, options)
  }

  static bulkBuild(valueSets, options) {
    options = Object.assign(
      {
        isNewRecord: true,
      },
      options || {}
    )

    if (!options.includeValidated) {
      this._conformIncludes(options, this)
      if (options.include) {
        this._expandIncludeAll(options)
        this._validateIncludedElements(options)
      }
    }

    if (options.attributes) {
      options.attributes = options.attributes.map(attribute =>
        (Array.isArray(attribute) ? attribute[1] : attribute))
    }

    return valueSets.map(values =>
      this.build(values, options))
  }

  static async findAll({ field, filter = '==', value, conditions }) {
    if (!field) {
      return this.db.collection(this.colName).get()
    }
    if (conditions) {
      return this.findWhere(conditions)
    }
    const ref = this.db.collection(this.colName).where(field, filter, value)
    return this.getDocuments(ref)
  }

  static async getDocuments(ref) {
    const docs = await ref.get()
    if (docs.empty) {
      console.log('No matching documents.')
      return false
    }

    return docs.map(doc =>
      this.build(doc.data(), { isNewRecord: false, raw: true }))
  }

  static async findById(id) {
    const ref = this.db.collection(this.colName).doc(id)
    const doc = await ref.get()
    if (doc.exists) {
      const data = doc.data()
      return this.build({ ...data, id }, { isNewRecord: false, raw: true })
    }
    return false
  }

  static async findWhere(conditions) {
    let ref = this.db.collection(this.colName)
    conditions.forEach(({ field, value, filter = '==' }) => {
      ref = ref.where(field, filter, value)
    })
    return this.getDocuments(ref)
  }

  static get colName() {
    const instance = new this()
    return instance.getDocumentName()
  }

  /**
   * This is the same as calling `set` and then calling `save` but it only saves the
   * exact values passed to it, making it more atomic and safer.
   *
   * @see
   * {@link Model#set}
   * @see
   * {@link Model#save}
   *
   * @param {Object} values See `set`
   * @param {Object} options See `save`
   *
   * @returns {Promise<Model>}
   */
  static update(values, options) {
    // Clone values so it doesn't get modified for caller scope and ignore undefined values
    values = _.omitBy(values, value =>
      value === undefined)

    const changedBefore = this.changed() || []

    options = options || {}
    if (Array.isArray(options)) options = { fields: options }

    options = Utils.cloneDeep(options)
    const setOptions = Utils.cloneDeep(options)
    setOptions.attributes = options.fields
    this.set(values, setOptions)

    // Now we need to figure out which fields were actually affected by the setter.
    const sideEffects = _.without(this.changed(), ...changedBefore)
    const fields = _.union(Object.keys(values), sideEffects)

    if (!options.fields) {
      options.fields = _.intersection(fields, this.changed())
      options.defaultFields = options.fields
    }

    return this.save(options)
  }

  /**
   * Validate the attributes of this instance according to validation rules set in the model definition.
   *
   * The promise fulfills if and only if validation successful; otherwise it rejects an Error instance containing { field name : [error msgs] } entries.
   *
   * @param {Object} [options] Options that are passed to the validator
   * @param {Array} [options.skip] An array of strings. All properties that are in this array will not be validated
   * @param {Array} [options.fields] An array of strings. Only the properties that are in this array will be validated
   * @param {boolean} [options.hooks=true] Run before and after validate hooks
   *
   * @returns {Promise}
   */
  async validate(options) {
    const iv = new InstanceValidator(this, options)
    return await iv.validate()
  }

  /**
   * Validate this instance, and if the validation passes, persist it to the database. It will only save changed fields, and do nothing if no fields have changed.
   *
   * On success, the callback will be called with this instance. On validation error, the callback will be called with an instance of `ValidationError`.
   * This error will have a property for each of the fields for which validation failed, with the error message for that field.
   *
   * @param {Object}      [options] save options
   * @param {string[]}    [options.fields] An optional array of strings, representing database columns. If fields is provided, only those columns will be validated and saved.
   * @param {boolean}     [options.silent=false] If true, the updatedAt timestamp will not be updated.
   * @param {boolean}     [options.validate=true] If false, validations won't be run.
   * @param {boolean}     [options.hooks=true] Run before and after create / update + validate hooks
   * @param {Function}    [options.logging=false] A function that gets executed while running the query to log the sql.
   * @param {Transaction} [options.transaction] Transaction to run query under
   * @param {string}      [options.searchPath=DEFAULT] An optional parameter to specify the schema search_path (Postgres only)
   * @param {boolean}     [options.returning] Append RETURNING * to get back auto generated values (Postgres only)
   *
   * @returns {Promise<Model>}
   */
  async save(options) {
    options = Utils.cloneDeep(options)
    options = _.defaults(options, {
      // hooks: true,
      validate: true,
    })

    if (!options.fields) {
      if (this.isNewRecord) {
        options.fields = Object.keys(this.constructor.rawAttributes)
      } else {
        options.fields = _.intersection(
          this.changed(),
          Object.keys(this.constructor.rawAttributes)
        )
      }

      options.defaultFields = options.fields
    }

    const primaryKeyName = this.constructor.primaryKeyAttribute
    const primaryKeyAttribute = primaryKeyName && this.constructor.rawAttributes[primaryKeyName]
    const createdAtAttr = this.constructor._timestampAttributes.createdAt
    const versionAttr = this.constructor._versionAttribute
    const wasNewRecord = this.isNewRecord

    const updatedAtAttr = this.constructor._timestampAttributes.updatedAt
    if (
      updatedAtAttr
      && options.fields.length >= 1
      && !options.fields.includes(updatedAtAttr)
    ) {
      options.fields.push(updatedAtAttr)
    }
    if (
      versionAttr
      && options.fields.length >= 1
      && !options.fields.includes(versionAttr)
    ) {
      options.fields.push(versionAttr)
    }

    if (this.isNewRecord === true) {
      if (createdAtAttr && !options.fields.includes(createdAtAttr)) {
        options.fields.push(createdAtAttr)
      }

      if (
        primaryKeyAttribute
        && primaryKeyAttribute.defaultValue
        && !options.fields.includes(primaryKeyName)
      ) {
        options.fields.unshift(primaryKeyName)
      }
    }

    if (
      updatedAtAttr
      && !options.silent
      && options.fields.includes(updatedAtAttr)
    ) {
      this.dataValues[updatedAtAttr] = new Date().toJSON()
    }

    if (this.isNewRecord && createdAtAttr && !this.dataValues[createdAtAttr]) {
      this.dataValues[createdAtAttr] = new Date().toJSON()
    }

    // Validate document
    const validationResults = await this.validate(options)
    if (validationResults.errors) {
      return Promise.reject(validationResults.errors.map(err =>
        err.message))
    }

    const values = Utils.mapValueFieldNames(
      this.dataValues,
      options.fields,
      this.constructor
    )

    // Transfer database generated values (defaults, autoincrement, etc)
    for (const attr of Object.keys(this.constructor.rawAttributes)) {
      if (
        this.constructor.rawAttributes[attr].field
        && values[this.constructor.rawAttributes[attr].field] !== undefined
        && this.constructor.rawAttributes[attr].field !== attr
      ) {
        values[attr] = values[this.constructor.rawAttributes[attr].field]
        delete values[this.constructor.rawAttributes[attr].field]
      }
    }

    const ref = this.constructor.db.collection(this.getDocumentName())
    if (this.isNewRecord) {
      const response = await ref.add(values)
      this.dataValues.id = this.id = response.id
    } else if (Object.keys(this._changed).length > 0) {
      await ref.doc(this.id).update(values)
    }

    this.isNewRecord = false
    return this
  }

  /**
   * If changed is called with a string it will return a boolean indicating whether the value of that key in `dataValues` is different from the value in `_previousDataValues`.
   *
   * If changed is called without an argument, it will return an array of keys that have changed.
   *
   * If changed is called without an argument and no keys have changed, it will return `false`.
   *
   * @param {string} [key] key to check or change status of
   * @param {any} [value] value to set
   *
   * @returns {boolean|Array}
   */
  changed(key, value) {
    if (key) {
      if (value !== undefined) {
        this._changed[key] = value
        return this
      }
      return this._changed[key] || false
    }

    const changed = Object.keys(this.dataValues).filter(key =>
      this.changed(key))

    return changed.length ? changed : false
  }
}

module.exports = Model
