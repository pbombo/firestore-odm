const util = require('util')
const _ = require('lodash')
const momentTz = require('moment-timezone')
const moment = require('moment')
const Validator = require('./validator-extras').validator

const warnings = {}

class ABSTRACT {
  stringify(value, options) {
    if (this._stringify) {
      return this._stringify(value, options)
    }
    return value
  }

  bindParam(value, options) {
    if (this._bindParam) {
      return this._bindParam(value, options)
    }
    return options.bindParam(this.stringify(value, options))
  }

  static toString() {
    return this.name
  }

  static warn(link, text) {
    if (!warnings[text]) {
      warnings[text] = true
      logger.warn(`${text} \n>> Check: ${link}`)
    }
  }

  static extend(oldType) {
    return new this(oldType.options)
  }
}

ABSTRACT.prototype.dialectTypes = ''

/**
 * STRING A variable length string
 */
/**
 * Unlimited length TEXT column
 */
class STRING extends ABSTRACT {
  toSql() {
    return 'string'
  }

  validate(value) {
    if (typeof value !== 'string') {
      throw new sequelizeErrors.ValidationError(
        util.format('%j is not a valid string', value),
      )
    }
    return true
  }
}

/**
 * Base number type which is used to build other types
 */
class NUMBER extends ABSTRACT {
  /**
   * @param {Object} options type options
   * @param {string|number} [options.length] length of type, like `INT(4)`
   * @param {boolean} [options.zerofill] Is zero filled?
   * @param {boolean} [options.unsigned] Is unsigned?
   * @param {string|number} [options.decimals] number of decimal points, used with length `FLOAT(5, 4)`
   * @param {string|number} [options.precision] defines precision for decimal type
   * @param {string|number} [options.scale] defines scale for decimal type
   */
  constructor(options = {}) {
    super()
    if (typeof options === 'number') {
      options = {
        length: options,
      }
    }
    this.options = options
    this._length = options.length
    this._zerofill = options.zerofill
    this._decimals = options.decimals
    this._precision = options.precision
    this._scale = options.scale
    this._unsigned = options.unsigned
  }

  toSql() {
    let result = this.key
    if (this._length) {
      result += `(${this._length}`
      if (typeof this._decimals === 'number') {
        result += `,${this._decimals}`
      }
      result += ')'
    }
    if (this._unsigned) {
      result += ' UNSIGNED'
    }
    if (this._zerofill) {
      result += ' ZEROFILL'
    }
    return result
  }

  validate(value) {
    if (!Validator.isFloat(String(value))) {
      throw new ValidationError(
        util.format(`%j is not a valid ${this.key.toLowerCase()}`, value),
      )
    }
    return true
  }

  _stringify(number) {
    if (
      typeof number === 'number'
      || typeof number === 'boolean'
      || number === null
      || number === undefined
    ) {
      return number
    }
    if (typeof number.toString === 'function') {
      return number.toString()
    }
    return number
  }

  get UNSIGNED() {
    this._unsigned = true
    this.options.unsigned = true
    return this
  }

  get ZEROFILL() {
    this._zerofill = true
    this.options.zerofill = true
    return this
  }

  static get UNSIGNED() {
    return new this().UNSIGNED
  }

  static get ZEROFILL() {
    return new this().ZEROFILL
  }
}

/**
 * A 32 bit integer
 */
class INTEGER extends NUMBER {
  validate(value) {
    if (!Validator.isInt(String(value))) {
      throw new ValidationError(
        util.format(`%j is not a valid ${this.key.toLowerCase()}`, value),
      )
    }
    return true
  }
}

/**
 * Floating point number (4-byte precision).
 */
class FLOAT extends NUMBER {
  /**
   * @param {string|number} [length] length of type, like `FLOAT(4)`
   * @param {string|number} [decimals] number of decimal points, used with length `FLOAT(5, 4)`
   */
  constructor(length, decimals) {
    super((typeof length === 'object' && length) || { length, decimals })
  }

  validate(value) {
    if (!Validator.isFloat(String(value))) {
      throw new sequelizeErrors.ValidationError(
        util.format('%j is not a valid float', value),
      )
    }
    return true
  }
}

// TODO: Create intermediate class
const protoExtensions = {
  escape: false,
  _value(value) {
    if (isNaN(value)) {
      return 'NaN'
    }
    if (!isFinite(value)) {
      const sign = value < 0 ? '-' : ''
      return `${sign}Infinity`
    }

    return value
  },
  _stringify(value) {
    return `'${this._value(value)}'`
  },
  _bindParam(value, options) {
    return options.bindParam(this._value(value))
  },
}

// for (const floating of [FLOAT, DOUBLE, REAL]) {
//     Object.assign(floating.prototype, protoExtensions);
// }

/**
 * A boolean
 */
class BOOLEAN extends ABSTRACT {
  toSql() {
    return 'BOOLEAN'
  }

  validate(value) {
    if (!Validator.isBoolean(String(value))) {
      throw new sequelizeErrors.ValidationError(
        util.format('%j is not a valid boolean', value),
      )
    }
    return true
  }

  _sanitize(value) {
    if (value !== null && value !== undefined) {
      if (Buffer.isBuffer(value) && value.length === 1) {
        // Bit fields are returned as buffers
        value = value[0]
      }
      const type = typeof value
      if (type === 'string') {
        // Only take action on valid boolean strings.
        return value === 'true' ? true : value === 'false' ? false : value
      }
      if (type === 'number') {
        // Only take action on valid boolean integers.
        return value === 1 ? true : value === 0 ? false : value
      }
    }
    return value
  }
}

BOOLEAN.parse = BOOLEAN.prototype._sanitize

/**
 * A time column
 *
 */
class TIME extends ABSTRACT {
  toSql() {
    return 'TIME'
  }
}

/**
 * Date column with timezone, default is UTC
 */
class DATE extends ABSTRACT {
  /**
   * @param {string|number} [length] precision to allow storing milliseconds
   */
  constructor(length) {
    super()
    const options = (typeof length === 'object' && length) || { length }
    this.options = options
    this._length = options.length || ''
  }

  validate(value) {
    if (!Validator.isDate(String(value))) {
      throw new ValidationError(util.format('%j is not a valid date', value))
    }
    return true
  }

  _sanitize(value, options) {
    if (
      (!options || (options && !options.raw))
      && !(value instanceof Date)
      && !!value
    ) {
      return new Date(value)
    }
    return value
  }

  _isChanged(value, originalValue) {
    if (
      originalValue
      && !!value
      && (value === originalValue
        || (value instanceof Date
          && originalValue instanceof Date
          && value.getTime() === originalValue.getTime()))
    ) {
      return false
    }
    // not changed when set to same empty value
    if (!originalValue && !value && originalValue === value) {
      return false
    }
    return true
  }

  _applyTimezone(date, options) {
    if (options.timezone) {
      if (momentTz.tz.zone(options.timezone)) {
        return momentTz(date).tz(options.timezone)
      }
      return (date = moment(date).utcOffset(options.timezone))
    }
    return momentTz(date)
  }

  _stringify(date, options) {
    date = this._applyTimezone(date, options)
    // Z here means current timezone, _not_ UTC
    return date.format('YYYY-MM-DD HH:mm:ss.SSS Z')
  }
}

/**
 * A date only column (no timestamp)
 */
class DATEONLY extends ABSTRACT {
  toSql() {
    return 'DATE'
  }

  _stringify(date) {
    return moment(date).format('YYYY-MM-DD')
  }

  _sanitize(value, options) {
    if ((!options || (options && !options.raw)) && !!value) {
      return moment(value).format('YYYY-MM-DD')
    }
    return value
  }

  _isChanged(value, originalValue) {
    if (originalValue && !!value && originalValue === value) {
      return false
    }
    // not changed when set to same empty value
    if (!originalValue && !value && originalValue === value) {
      return false
    }
    return true
  }
}

/**
 * A JSON string column
 */
class MAP extends ABSTRACT {
  validate() {
    return true
  }

  _stringify(value) {
    return JSON.stringify(value)
  }
}

/**
 * A default value of the current timestamp
 */
class NOW extends ABSTRACT {
  validate(value) {
    moment(value).isValid()
  }
}

/**
 * A column storing a unique universal identifier.
 * Use with `UUIDV1` or `UUIDV4` for default values.
 */
class UUID extends ABSTRACT {
  validate(value, options) {
    if (
      typeof value !== 'string'
      || (!Validator.isUUID(value) && (!options || !options.acceptStrings))
    ) {
      throw new sequelizeErrors.ValidationError(
        util.format('%j is not a valid uuid', value),
      )
    }
    return true
  }
}

/**
 * A default unique universal identifier generated following the UUID v1 standard
 */
class UUIDV1 extends ABSTRACT {
  validate(value, options) {
    if (
      typeof value !== 'string'
      || (!Validator.isUUID(value) && (!options || !options.acceptStrings))
    ) {
      throw new sequelizeErrors.ValidationError(
        util.format('%j is not a valid uuid', value),
      )
    }
    return true
  }
}

/**
 * A default unique universal identifier generated following the UUID v4 standard
 */
class UUIDV4 extends ABSTRACT {
  validate(value, options) {
    if (
      typeof value !== 'string'
      || (!Validator.isUUID(value, 4) && (!options || !options.acceptStrings))
    ) {
      throw new sequelizeErrors.ValidationError(
        util.format('%j is not a valid uuidv4', value),
      )
    }
    return true
  }
}

/**
 * A virtual value that is not stored in the DB. This could for example be useful if you want to provide a default value in your model that is returned to the user but not stored in the DB.
 *
 * You could also use it to validate a value before permuting and storing it. VIRTUAL also takes a return type and dependency fields as arguments
 * If a virtual attribute is present in `attributes` it will automatically pull in the extra fields as well.
 * Return type is mostly useful for setups that rely on types like GraphQL.
 *
 * @example <caption>Checking password length before hashing it</caption>
 * sequelize.define('user', {
 *   password_hash: DataTypes.STRING,
 *   password: {
 *     type: DataTypes.VIRTUAL,
 *     set: function (val) {
 *        // Remember to set the data value, otherwise it won't be validated
 *        this.setDataValue('password', val);
 *        this.setDataValue('password_hash', this.salt + val);
 *      },
 *      validate: {
 *         isLongEnough: function (val) {
 *           if (val.length < 7) {
 *             throw new Error("Please choose a longer password")
 *          }
 *       }
 *     }
 *   }
 * })
 *
 * # In the above code the password is stored plainly in the password field so it can be validated, but is never stored in the DB.
 *
 * @example <caption>Virtual with dependency fields</caption>
 * {
 *   active: {
 *     type: new DataTypes.VIRTUAL(DataTypes.BOOLEAN, ['createdAt']),
 *     get: function() {
 *       return this.get('createdAt') > Date.now() - (7 * 24 * 60 * 60 * 1000)
 *     }
 *   }
 * }
 *
 */
class VIRTUAL extends ABSTRACT {
  /**
   * @param {ABSTRACT} [ReturnType] return type for virtual type
   * @param {Array} [fields] array of fields this virtual type is dependent on
   */
  constructor(ReturnType, fields) {
    super()
    if (typeof ReturnType === 'function') ReturnType = new ReturnType()
    this.returnType = ReturnType
    this.fields = fields
  }
}

/**
 * An array of `type`. Only available in Postgres.
 *
 * @example
 * DataTypes.ARRAY(DataTypes.DECIMAL)
 */
class ARRAY extends ABSTRACT {
  /**
   * @param {ABSTRACT} type type of array values
   */
  constructor(type) {
    super()
    const options = _.isPlainObject(type) ? type : { type }
    this.options = options
    this.type = typeof options.type === 'function' ? new options.type() : options.type
  }

  toSql() {
    return `${this.type.toSql()}[]`
  }

  validate(value) {
    if (!Array.isArray(value)) {
      throw new ValidationError(util.format('%j is not a valid array', value))
    }
    return true
  }

  static is(obj, type) {
    return obj instanceof ARRAY && obj.type instanceof type
  }
}

/**
 * A column storing Geometry information.
 *
 * GeoPOINT is accepted as input and returned as output.
 *
 * @example <caption>Defining a Geometry type attribute</caption>
 * DataTypes.GEOPOINT
 *
 * @example <caption>Create a new point</caption>
 * const point = { x: 39.807222, y: -76.984722};
 *
 * User.create({username: 'username', location: point });
 *
 */
class GEOPOINT extends ABSTRACT {
  /**
   * @param {string} [type] Type of geometry data
   */
  constructor(type) {
    super()
    const options = { type }
    this.options = options
    this.type = options.type
  }

  _stringify(value) {
    return `${value.x},${value.y}`
  }
}

/**
 * A column storing Geometry information.
 *
 * GeoJSON is accepted as input and returned as output.
 *
 * @example <caption>Defining a Geometry type attribute</caption>
 * DataTypes.GEOMETRY
 * DataTypes.GEOMETRY('POINT')
 * DataTypes.GEOMETRY('POINT', 4326)
 *
 * @example <caption>Create a new point</caption>
 * const point = { type: 'Point', coordinates: [39.807222,-76.984722]};
 *
 * User.create({username: 'username', geometry: point });
 *
 *
 *
 * @see {@link DataTypes.GEOGRAPHY}
 */
class GEOMETRY extends ABSTRACT {
  /**
   * @param {string} [type] Type of geometry data
   */
  constructor(type) {
    super()
    const options = { type }
    this.options = options
    this.type = options.type
  }
  // _stringify(value, options) {
  //     return `GeomFromText(${options.escape(wkx.Geometry.parseGeoJSON(value).toWkt())})`;
  // }
  // _bindParam(value, options) {
  //     return `GeomFromText(${options.bindParam(wkx.Geometry.parseGeoJSON(value).toWkt())})`;
  // }
}

GEOMETRY.prototype.escape = false

/**
 * A convenience class holding commonly used data types. The data types are used when defining a new model using `Sequelize.define`, like this:
 * ```js
 * sequelize.define('model', {
 *   column: DataTypes.INTEGER
 * })
 * ```
 * When defining a model you can just as easily pass a string as type, but often using the types defined here is beneficial. For example, using `DataTypes.BLOB`, mean
 * that that column will be returned as an instance of `Buffer` when being fetched by sequelize.
 *
 * To provide a length for the data type, you can invoke it like a function: `INTEGER(2)`
 *
 * Some data types have special properties that can be accessed in order to change the data type.
 * For example, to get an unsigned integer with zerofill you can do `DataTypes.INTEGER.UNSIGNED.ZEROFILL`.
 * The order you access the properties in do not matter, so `DataTypes.INTEGER.ZEROFILL.UNSIGNED` is fine as well.
 *
 * * All number types (`INTEGER`, `BIGINT`, `FLOAT`, `DOUBLE`, `REAL`, `DECIMAL`) expose the properties `UNSIGNED` and `ZEROFILL`
 * * The `CHAR` and `STRING` types expose the `BINARY` property
 *
 * Three of the values provided here (`NOW`, `UUIDV1` and `UUIDV4`) are special default values, that should not be used to define types. Instead they are used as shorthands for
 * defining default values. For example, to get a uuid field with a default value generated following v1 of the UUID standard:
 * ```js`
 * sequelize.define('model',` {
 *   uuid: {
 *     type: DataTypes.UUID,
 *     defaultValue: DataTypes.UUIDV1,
 *     primaryKey: true
 *   }
 * })
 * ```
 * There may be times when you want to generate your own UUID conforming to some other algorithm. This is accomplished
 * using the defaultValue property as well, but instead of specifying one of the supplied UUID types, you return a value
 * from a function.
 * ```js
 * sequelize.define('model', {
 *   uuid: {
 *     type: DataTypes.UUID,
 *     defaultValue: function() {
 *       return generateMyId()
 *     },
 *     primaryKey: true
 *   }
 * })
 * ```
 */
module.exports = {
  ABSTRACT,
  STRING,
  NUMBER,
  INTEGER,
  FLOAT,
  TIME,
  DATE,
  DATEONLY,
  BOOLEAN,
  NOW,
  UUID,
  UUIDV1,
  UUIDV4,
  MAP,
  VIRTUAL,
  ARRAY,
  GEOMETRY,
  GEOPOINT,
}
