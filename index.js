/**
 * The entry point.
 *
 * @module FirestormODM
 */
module.exports.Model = require('./lib/model')
module.exports.DataTypes = require('./lib/data-types')
module.exports.validate = require('./lib/validator-extras').validator
